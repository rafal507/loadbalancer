package com.open.loadbalancer.service;

import com.open.loadbalancer.domain.Request;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static com.open.loadbalancer.domain.AlgorithmType.FIRST_UNDER_LIMIT_OR_THE_LOWEST_LOAD;
import static com.open.loadbalancer.domain.AlgorithmType.SEQUENTIALLY_IN_ROTATION;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class LoadBalancerTest {

    private Balancer balancer;
    private Request request;

    @BeforeEach
    void setUp() {
        request = new Request();
    }

    @Test
    @DisplayName("Should handle request sequentially in rotation")
    void shouldHandleRequestSequentiallyInRotation() {
        final List<HostInstance> hostInstances = getDefaultHostInstances();
        balancer = new LoadBalancer(hostInstances, SEQUENTIALLY_IN_ROTATION);

        assertEquals(0.78f, balancer.handleRequest(request));
        assertEquals(0.65f, balancer.handleRequest(request));
        assertEquals(0.75f, balancer.handleRequest(request));
        assertEquals(0.78f, balancer.handleRequest(request));
    }

    @Test
    @DisplayName("Should handle request by the first host under Load")
    void shouldHandleRequestByFirstHostUnderLoad() {
        final List<HostInstance> hostInstances = getDefaultHostInstances();
        balancer = new LoadBalancer(hostInstances, FIRST_UNDER_LIMIT_OR_THE_LOWEST_LOAD);

        assertEquals(0.65f, balancer.handleRequest(request));
        assertEquals(0.65f, balancer.handleRequest(request));
    }

    @Test
    @DisplayName("Should handle request by the host with lowest load")
    void shouldHandleRequestByHostWithTheLowestLoad() {
        final List<HostInstance> hostInstances = Arrays.asList(new HostInstance(0.88f),
            new HostInstance(0.92f), new HostInstance(0.77f), new HostInstance(0.86f));
        balancer = new LoadBalancer(hostInstances, FIRST_UNDER_LIMIT_OR_THE_LOWEST_LOAD);

        assertEquals(0.77f, balancer.handleRequest(request));
        assertEquals(0.77f, balancer.handleRequest(request));
    }

    @Test
    @DisplayName("Should throw exception when list of hosts is null")
    void shouldThrowExceptionWhenListOfHostsIsNull() {
        balancer = new LoadBalancer(null, SEQUENTIALLY_IN_ROTATION);
        final Request request = new Request();

        assertThrows(IllegalStateException.class, () -> balancer.handleRequest(request), "The list of available hosts is empty");
    }

    @Test
    @DisplayName("Should throw exception when list of hosts is empty")
    void shouldThrowExceptionWhenListOfHostsIsEmpty() {
        balancer = new LoadBalancer(Collections.emptyList(), SEQUENTIALLY_IN_ROTATION);

        assertThrows(IllegalStateException.class, () -> balancer.handleRequest(request), "The list of available hosts is empty");
    }

    @Test
    @DisplayName("Should throw exception when algorithm variant not supported")
    void shouldThrowExceptionWhenAlgorithmNotSupported() {
        balancer = new LoadBalancer(getDefaultHostInstances(), null);

        assertThrows(IllegalStateException.class, () -> balancer.handleRequest(request), "The algorithm variant is empty");
    }

    private List<HostInstance> getDefaultHostInstances() {
        return Arrays.asList(new HostInstance(0.78f), new HostInstance(0.65f),
            new HostInstance(0.75f));
    }
}