package com.open.loadbalancer.domain;

public enum AlgorithmType {
    SEQUENTIALLY_IN_ROTATION,
    FIRST_UNDER_LIMIT_OR_THE_LOWEST_LOAD
}
