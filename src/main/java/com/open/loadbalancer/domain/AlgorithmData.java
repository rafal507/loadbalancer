package com.open.loadbalancer.domain;

import com.open.loadbalancer.service.HostInstance;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AlgorithmData {
    private Request request;
    private List<HostInstance> hostInstances;
    private int index;
}
