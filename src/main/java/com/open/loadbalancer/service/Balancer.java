package com.open.loadbalancer.service;

import com.open.loadbalancer.domain.Request;

public interface Balancer {
    float handleRequest(Request request);
}
