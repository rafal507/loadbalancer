package com.open.loadbalancer.service;

import com.open.loadbalancer.domain.AlgorithmData;
import com.open.loadbalancer.domain.AlgorithmType;
import com.open.loadbalancer.domain.Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import static com.open.loadbalancer.domain.AlgorithmType.SEQUENTIALLY_IN_ROTATION;
import static java.util.Objects.isNull;
import static org.springframework.util.CollectionUtils.isEmpty;

public class LoadBalancer implements Balancer {
    private static final Logger LOG = LoggerFactory.getLogger(LoadBalancer.class);

    private final AlgorithmType algorithmType;
    private final AlgorithmData algorithmData;

    public LoadBalancer(final List<HostInstance> hostInstances, final AlgorithmType algorithmType) {
        this.algorithmType = algorithmType;
        this.algorithmData = AlgorithmData.builder()
            .hostInstances(hostInstances)
            .index(0)
            .build();
    }

    @Override
    public float handleRequest(final Request request) {
        validateHostsAndAlgorithmVariant();
        algorithmData.setRequest(request);

        if (SEQUENTIALLY_IN_ROTATION.equals(algorithmType)) {
            return BalancerStrategy.sequentiallyInRotation().execute(algorithmData);
        }

        return BalancerStrategy.firstUnderLimitOrTheLowestLoad().execute(algorithmData);
    }

    private void validateHostsAndAlgorithmVariant() {
        if (isEmpty(algorithmData.getHostInstances())) {
            throwIllegalStateException("The list of available hosts is empty");
        }
        if (isNull(algorithmType)) {
            throwIllegalStateException("The algorithm variant is empty");
        }
    }

    private void throwIllegalStateException(final String message) {
        LOG.error(message);
        throw new IllegalStateException(message);
    }

}
