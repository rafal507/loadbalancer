package com.open.loadbalancer.service;

import com.open.loadbalancer.domain.AlgorithmData;

import java.util.Comparator;
import java.util.List;

public interface BalancerStrategy {

    float LOAD_LIMIT = 0.75F;

    float execute(final AlgorithmData algorithmData);

    static BalancerStrategy sequentiallyInRotation() {
        return BalancerStrategy::executeSequentiallyInRotation;
    }

    static BalancerStrategy firstUnderLimitOrTheLowestLoad() {
        return BalancerStrategy::executeFirstUnderLimitOrTheLowestLoad;
    }

    private static float executeSequentiallyInRotation(AlgorithmData algorithmData) {
        int index = algorithmData.getIndex();
        final HostInstance hostInstance = algorithmData.getHostInstances().get(index);
        hostInstance.handleRequest(algorithmData.getRequest());
        index++;
        if (index == algorithmData.getHostInstances().size()) {
            index = 0;
        }
        algorithmData.setIndex(index);
        return hostInstance.getLoad();
    }

    private static float executeFirstUnderLimitOrTheLowestLoad(AlgorithmData algorithmData) {
        final List<HostInstance> hostInstances = algorithmData.getHostInstances();
        final HostInstance hostInstance = hostInstances.stream()
            .filter(host -> host.getLoad() < LOAD_LIMIT)
            .findFirst()
            .orElseGet(() -> getHostWithTheLowestLoad(hostInstances));
        hostInstance.handleRequest(algorithmData.getRequest());
        return hostInstance.getLoad();
    }

    private static HostInstance getHostWithTheLowestLoad(final List<HostInstance> hostInstances) {
        return hostInstances.stream()
            .min(Comparator.comparingDouble(HostInstance::getLoad))
            .get();
    }
}
