package com.open.loadbalancer.service;

import com.open.loadbalancer.domain.Request;


public class HostInstance {

    private float load;

    public HostInstance(final float load) {
        this.load = load;
    }

    public float getLoad() {
        return this.load;
    }

    public void handleRequest(final Request request) {

    }

}
